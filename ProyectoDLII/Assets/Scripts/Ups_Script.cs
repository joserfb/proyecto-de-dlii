﻿using UnityEngine;
using System.Collections;

public class Ups_Script : MonoBehaviour {

	public string StaticChange;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (0, 1, 0);
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Pla_Tag") {
			switch (StaticChange) {
			case "Ups_Mini":
				Gen_Gamemaster.instance.Ups_MiniUse =true;
				break;
			case "Ups_Tanker":
				Gen_Gamemaster.instance.Ups_TankerUse =true;
				break;
			case "Ups_Polary":
				Gen_Gamemaster.instance.Ups_PolaryUse =true;
				break;
			}
			Instantiate(Gen_Gamemaster.instance.Gen_Disapear, this.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
		}
	}
}
