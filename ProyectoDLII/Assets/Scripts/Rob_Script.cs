﻿using UnityEngine;
using System.Collections;

public class Rob_Script : MonoBehaviour {

	public string StaticChange;
	private GameObject go;
	public GameObject player_robot;
	private Vector3 pos;
	private Quaternion rot;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Pla_Tag") {
			Gen_Gamemaster.instance.actual_robot_pos = transform.position;
			Gen_Gamemaster.instance.actual_robot_rot = transform.rotation;
			switch (StaticChange) {
			case "Ups_Mini":
				if(Gen_Gamemaster.instance.Ups_MiniUse == true) {
					Gen_Gamemaster.instance.Pla_Mode = "Rob_Mini";
					Destroy(this.gameObject);
					GameObject tmp = (GameObject) Instantiate(player_robot, pos, rot);
					tmp.transform.parent = other.transform;
					tmp.transform.localPosition = new Vector3(0, 0, 0);
				}
				break;
			case "Ups_Tanker":
				if(Gen_Gamemaster.instance.Ups_TankerUse == true) {
					Gen_Gamemaster.instance.Pla_Mode = "Rob_Tanker";
					Destroy(this.gameObject);
					GameObject tmp = (GameObject) Instantiate(player_robot, pos, rot);
					tmp.transform.parent = other.transform;
					tmp.transform.localPosition = new Vector3(0, 1, 0);
				}
				break;
			case "Ups_Polary":
				if(Gen_Gamemaster.instance.Ups_PolaryUse == true){
					Gen_Gamemaster.instance.Pla_Mode = "Rob_Polary";
					Destroy(this.gameObject);
					GameObject tmp = (GameObject) Instantiate(player_robot, pos, rot);
					tmp.transform.parent = other.transform;
					tmp.transform.localPosition = new Vector3(0, 0.5f, -0.5f);
				}
				break;
			}
		}
	}
}
