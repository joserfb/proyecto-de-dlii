﻿using UnityEngine;
using System.Collections;

public class Pla_Script : MonoBehaviour {

	private Vector3 Pla_CameraPos;
	public float Pla_Speed =2.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float shi_horizontal = Input.GetAxisRaw ("Horizontal") * Pla_Speed * Time.deltaTime;
		float shi_vertical = Input.GetAxisRaw ("Vertical") * Pla_Speed  * Time.deltaTime;
		Pla_CameraPos = this.rigidbody.position;
		Pla_CameraPos.x += shi_horizontal;
		Pla_CameraPos.z += shi_vertical;
		this.rigidbody.MovePosition(Pla_CameraPos); 
		if ((Input.GetAxis ("Vertical") == 0.0f) && Input.GetAxis ("Horizontal") == 0.0f) {
			this.rigidbody.velocity = new Vector3(0, 0, 0);
		}
		
		Pla_CameraPos.y = Pla_CameraPos.y + 8.0f;
		Pla_CameraPos.z = Pla_CameraPos.z - 1.5f;
		Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, Pla_CameraPos, Time.deltaTime*5);
	}
}
