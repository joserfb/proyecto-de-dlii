﻿using UnityEngine;
using System.Collections;

public class GravityField : MonoBehaviour {
	private float speed;
	
	void Start () {
	
	}

	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other){
		speed = other.collider.GetComponent<MovementScript> ().speed;
		other.collider.GetComponent<MovementScript> ().speed /= 2.0f;
	}

	void OnTriggerExit(Collider other){
		other.collider.GetComponent<MovementScript> ().speed = speed;
	}
}
