﻿using UnityEngine;
using System.Collections;

public class Ass_Script : MonoBehaviour {


	public string StaticChange;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Gen_Gamemaster.instance.Pla_Mode == StaticChange)
		   collider.isTrigger = true;
		else
		   collider.isTrigger = false;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Pla_Tag") {
			switch (Gen_Gamemaster.instance.Pla_Mode) {
			case "Rob_Mini":
					Debug.Log ("You are mini");
				break;
			case "Rob_Tanker":
				collider.isTrigger = false;
				Destroy(this.gameObject);
				break;
			case "Rob_Polary":
					Debug.Log ("You are Polary");
				break;
			}
		}
	}

}
