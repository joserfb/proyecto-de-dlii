﻿using UnityEngine;
using System.Collections;

public class Gen_Gamemaster : MonoBehaviour
{
	private static Gen_Gamemaster _instance;

	public static Gen_Gamemaster instance {
		get {
			if (_instance == null) {
				_instance = GameObject.FindObjectOfType<Gen_Gamemaster> ();
				//Tell unity not to destroy this object when loading a new scene!
				DontDestroyOnLoad (_instance.gameObject);
			}

			return _instance;
		}
	}

	void Awake ()
	{
		if (_instance == null) {
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad (this);
		} else {
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if (this != _instance)
				Destroy (this.gameObject);
		}
	}

	public bool Ups_MiniUse = false;
	public bool Ups_TankerUse = false;
	public bool Ups_PolaryUse = false;

	public string Pla_Mode = "";
	public GameObject Gen_Disapear;

	public Vector3 actual_robot_pos;
	public Quaternion actual_robot_rot;

	public Object pref_hulker;
	public Object pref_mini;
	public Object pref_polary;
}
