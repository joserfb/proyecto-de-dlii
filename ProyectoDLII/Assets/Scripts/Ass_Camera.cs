﻿using UnityEngine;
using System.Collections;

public class Ass_Camera : MonoBehaviour {
	private GameObject go;
	//private GameObject parent;
	public bool left = true;
	public bool right = false;
	public float edge_left;
	public float edge_right;
	//public Object pref_hulker;
	//public Object pref_mini;
	//public Object pref_polary;
	// Use this for initialization
	void Start () {
		//parent = transform.parent.gameObject;
		//parent.transform.eulerAngles = new Vector3 (0, 90, 315);
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.eulerAngles.y < edge_left && left) {
			Vector3 temp = transform.eulerAngles;
			temp.y += 0.5f; 
			transform.eulerAngles = temp;
		}

		if (transform.eulerAngles.y > edge_right && right) {
			Vector3 temp = transform.eulerAngles;
			temp.y -= 0.5f; 
			transform.eulerAngles = temp;
		}

		if (transform.eulerAngles.y > edge_left - 1.0f) {
			left = false;
			right = true;
		}

		if (transform.eulerAngles.y < edge_right + 1.0f) {
			left = true;
			right = false;
		}

	}
		
	void OnTriggerEnter(Collider other) {
		if (other.tag == "Pla_Tag") {
			string last_mode = Gen_Gamemaster.instance.Pla_Mode;
			Gen_Gamemaster.instance.Pla_Mode = "";

			int childs = other.transform.childCount;

			if (childs > 1){
				Destroy(other.transform.GetChild(1).gameObject);	
				StartCoroutine("SpawnRobot", last_mode);
			}
		}
	}
	
	IEnumerator SpawnRobot(string mode) {
		yield return new WaitForSeconds (2);

		switch (mode) {
			case "Rob_Mini":
				
				break;
			case "Rob_Tanker":
				Instantiate(Gen_Gamemaster.instance.pref_hulker, 
			            Gen_Gamemaster.instance.actual_robot_pos,
			            Gen_Gamemaster.instance.actual_robot_rot);			
				break;
			case "Rob_Polary": 
				Instantiate(Gen_Gamemaster.instance.pref_polary, 
			            Gen_Gamemaster.instance.actual_robot_pos,
			            Gen_Gamemaster.instance.actual_robot_rot);
				break;
		}
	}

}
