﻿using UnityEngine;
using System.Collections;

public class Computer : MonoBehaviour {


	//public string StaticChange;
	public GameObject door;
	public GameObject cam;
	private GameObject go;
	public bool computerchecked = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Pla_Tag" && !computerchecked) {
			computerchecked = true;
			StartCoroutine(Open());
		}
	}

	IEnumerator Open() {
		cam.camera.enabled = true;
		yield return new WaitForSeconds (2);

		int steps = 35;
		for (int i = 0; i < steps; i++) {
			Vector3 temp = door.transform.position;
			temp.y -= 0.1f; 
			door.transform.position = temp;
			yield return null;
		}

		yield return new WaitForSeconds (3);
		cam.camera.enabled = false;
	}
}
