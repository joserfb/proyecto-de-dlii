using UnityEngine;
using System.Collections;

public class MovementScript : MonoBehaviour
{
	private Vector3 Pla_CameraPos;
	public float speed = 10.0f;
	CharacterController cc;
	public float y;
	public float z;
	
	void Awake()
	{
		cc = GetComponent<CharacterController>();
	}
	
	void FixedUpdate()
	{
		Vector3 move = Vector3.zero;
		move.x = Input.GetAxis("Horizontal") * speed;
		move.z = Input.GetAxis("Vertical") * speed;
		cc.SimpleMove(move);

		Pla_CameraPos = this.transform.position;
		//Pla_CameraPos.x += move.x;
		//Pla_CameraPos.z += move.z;

		Pla_CameraPos.y = Pla_CameraPos.y + y;
		Pla_CameraPos.z = Pla_CameraPos.z - z;
		Camera.main.transform.position = Vector3.Lerp (Camera.main.transform.position, Pla_CameraPos, Time.deltaTime*1);

	}	
}
